from trader import AlgoManager as AM
from datetime import datetime
import pandas as pd
import numpy as np
import csv
import argparse
import pdb
import matplotlib
# Allow plot over X11
matplotlib.use('tkagg')
from matplotlib import pyplot as plt
import itertools

from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression, Perceptron, LogisticRegressionCV
from sklearn.model_selection import cross_val_score, RepeatedKFold
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.metrics import accuracy_score, recall_score, precision_score, confusion_matrix, roc_curve
from sklearn.preprocessing import label_binarize

class MLAlgo(AM.Algorithm):
    def initialize(self):
        self.benchmark = "SPY"
        self.first_run = True
        self.num_up = 0
        self.num_down = 0
        self.equity_down = "TLT"
        self.equity_up = "SPY"
        self.prices = []
        self.last_feature = []
        self.features = []
        self.labels = []
        self.features_file = None
        self.labels_file = None
        self.features_cache = None
        self.labels_cache = None
        self.fc = 0

        #ML Parameters
        self.clf = None
        self.CLASS_FLAT = 0
        self.CLASS_UP = 1
        self.CLASS_DOWN = 2
        self.threshold = threshold
        self.online = False

    def run(self):
        global interval, regenerate
        if interval == "weekly" and self.datetime.weekday() != 0:
            #Do nothing except on Mondays
            return

        if self.first_run:
            self.prices = self.history("SPY", interval="daily", length=6, datatype="close").tolist()
            #TODO: Check if current price is included
            self.prices = self.prices[:-1]
            self.first_run = False

        if self.labels_cache is None:
            current_price = self.quote("SPY")
            last_price = self.prices[-1]
            y = [get_label(current_price, last_price)]
            self.labels.append(y)
            self.prices.append(current_price)
        elif self.fc > 0:
            y = labels_cache[self.fc - 1]

        if self.online and len(self.last_feature):
            print "Online learning: " + str(self.last_feature)
            print "Labels: " + str(y)
            self.clf.partial_fit(self.last_feature, y)

        #Reshape for single sample
        X_orig = None
        if self.features_cache is not None and len(self.features_cache) <= self.fc:
            return
        elif self.features_cache is not None:
            X_orig = self.features_cache[self.fc]
            self.fc += 1
        else:
            X_orig = get_features(self, current_price, self.prices)
            self.features.append(X_orig)
            #pdb.set_trace()
            #save(self, "t1.tmp", "t2.tmp")
        print "X_orig: " + str(X_orig)
        X = np.asarray(X_orig).reshape(1, -1)
        pred = self.clf.predict(X)[0]
        if pred == self.CLASS_UP:
            #self.orderpercent(self.equity_down, 0)
            self.orderpercent(self.equity_up, 1)
            self.num_up += 1
        elif pred == self.CLASS_DOWN:
            self.orderpercent(self.equity_up, 0)
            #self.orderpercent(self.equity_down, 1)
            self.num_down += 1

        self.last_feature = X


class MLTrainingAlgo(AM.Algorithm):
    def initialize(self):
        self.benchmark = "SPY"
        self.labels = []
        #self.features = pd.DataFrame(column=['pct_change_day', 'pct_change_5_days'])
        self.features = []
        self.prices = []
        self.warmup = 5

        #ML Parameters
        self.ml_threshold = threshold
        self.CLASS_FLAT = 0
        self.CLASS_UP = 1
        self.CLASS_DOWN = 2

    def run(self):
        global interval
        if interval == "weekly" and self.datetime.weekday() != 0:
            #Do nothing except on Mondays
            return

        current_price = self.quote("SPY")

        if self.warmup > 0:
            self.warmup -= 1
            self.prices.append(current_price)
            return

        #Calculate percent changes
        f = get_features(self, current_price, self.prices)
        d_1_day = f[0]
        self.features.append(f)
        
        #Add label for yesterday now that we have current price
        #Labels should be offset by 1
        if self.warmup < 0:
            l = None
            delta = d_1_day #Same calculation
            if delta < -self.ml_threshold:
                l = self.CLASS_DOWN
            elif delta > self.ml_threshold:
                l = self.CLASS_UP
            else:
                l = self.CLASS_FLAT
            self.labels.append(l)
        else:
            self.warmup -= 1

        #Commit calculations
        self.prices.append(current_price)
        
def get_bool(prompt):
    while True:
        try:
            return {'y':True,'n':False}[raw_input(prompt)]
        except KeyError:
            print "Invalid input please enter 'y' or 'n'"

def get_label(current_price, last_price):
    delta = (current_price - last_price) / last_price
    if delta < -threshold:
        l = CLASS_DOWN
    elif delta > threshold:
        l = CLASS_UP
    else:
        l = CLASS_FLAT
    return l

def get_features(algo, current_price, prices):
    #Read file if needed
    global spy_pe, last_stoch, interval
    if spy_pe is None:
        with open("spy-pe-ratio.csv", "r") as f:
            reader = csv.reader(f)
            spy_pe = {r[0] : r[1] for r in reader}
    
    d = str(algo.datetime.date())
    d = d[:-2] + "01"
    pe = float(spy_pe[d])

    #Calculate percent changes
    prev_price = np.mean(prices[-5:])
    d_5_days = (current_price - prev_price) / prev_price

    prev_price = prices[-1]
    d_1_day = (current_price - prev_price) / prev_price

    if interval == "weekly":
        divisor = 4
    else:
        divisor = 1

    print "Window = " + str(int(5/divisor)) + ", interval = " + str(interval)
    sma_5 = algo.sma("SPY", mawindow=int(4/divisor)+1, interval=interval)["SMA"].iloc[0]
    sma_21 = algo.sma("SPY", mawindow=int(21/divisor), interval=interval)["SMA"].iloc[0]
    sma_84 = algo.sma("SPY", mawindow=int(84/divisor), interval=interval)["SMA"].iloc[0]
    ema_5 = algo.ema("SPY", mawindow=int(4/divisor)+1, interval=interval)["EMA"].iloc[0]
    ema_21 = algo.ema("SPY", mawindow=int(21/divisor), interval=interval)["EMA"].iloc[0]
    ema_84 = algo.ema("SPY", mawindow=int(84/divisor), interval=interval)["EMA"].iloc[0]
    rsi = algo.rsi("SPY", interval=interval)["RSI"].iloc[0]
    bb = algo.bollinger("SPY", mawindow=int(21/divisor), interval=interval)
    bb_top = current_price / bb["top"].iloc[0]
    bb_bot = current_price / bb["bottom"].iloc[0]
    bb_mid = current_price / bb["middle"].iloc[0]
    macd = algo.macd("SPY", interval=interval)["signal"][0] #Use default, 12-26-9
    d = algo.datetime.strftime("%Y-%m-%d")
    try:
        stoch = algo.stoch("SPY", interval=interval)
        stoch_k = stoch["SlowK"].iloc[0]
        stoch_d = stoch["SlowD"].iloc[0]
        last_stoch = (stoch_k, stoch_d)
    except KeyError:
        print "Failed to match stoch, using last value"
        stoch_k, stoch_d = last_stoch

    l = [d_1_day, d_5_days, sma_5, sma_21, sma_84, ema_5, ema_21, ema_84, rsi, bb_top, bb_bot, bb_mid, macd, pe, stoch_k, stoch_d]
    #print l
    return l


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

def save(algo, f_name, l_name, p_name):
    #Train the model
    X = np.asarray(algo.features[:-1])
    y = np.asarray(algo.labels)
    np.savetxt(f_name, X, delimiter=",")
    np.savetxt(l_name, y, delimiter=",")
    np.savetxt(p_name, p, delimiter=",")

if __name__ == '__main__':
    global spy_pe
    global threshold
    global CLASS_FLAT
    global CLASS_UP
    global CLASS_DOWN
    global verbose
    global interval
    global regenerate
    global num_features
    num_features = 16 #default to all features

    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
    parser.add_argument("-r", "--regenerate", help="discard cache and regenerate data", action="store_true")
    parser.add_argument("-i", "--interval", help="interval for predictions, possible values are `daily` (default), `weekly`, `monthly`", default="daily")
    parser.add_argument("-c", "--clf", help="classifier to train, possible values are `all` (default), `log-regression`, `perceptron`, `random-forest`, `svc-rbf`", default="all")
    parser.add_argument("-o", "--output", help="dump classifier training stats for analysis", default="results.csv")
    parser.add_argument("-a", "--analysis", help="output file for training results", action="store_true")
    parser.add_argument("-m", "--confusion-matrix", help="display confusion matrix graph of final classifier", action="store_true")
    parser.add_argument("--roc", help="display ROC curve of final classifier", action="store_true")
    parser.add_argument("-f", "--features", help="number of features to use", default=num_features, type=int)
    parser.add_argument("--min-features", help="if specified with --features, analysis will be performed over range from --min-features to --features. Otherwise no affect.", default=None, type=int)
    parser.add_argument("--no-test", help="skip out of sample test", action="store_true")
    args = parser.parse_args()
    verbose = args.verbose
    interval = args.interval
    regenerate = args.regenerate
    num_features = args.features
    if args.min_features is not None:
        iterate_features = xrange(args.min_features, args.features + 1)
    else:
        iterate_features = [args.features]

    if verbose:
        print "Program initialized"
    spy_pe = None
    if interval == "weekly":
        threshold = .005
    else:
        threshold = .0035
    CLASS_FLAT = 0
    CLASS_UP = 1
    CLASS_DOWN = 2

    features_name = "features-" + interval
    labels_name = "labels-" + interval
    ext = ".csv"

    if args.regenerate:
        if verbose:
            print "Regenerating data"
        #Generate training data
        train_algo = MLTrainingAlgo(times=['every day'])
        train_algo_back = AM.backtester(train_algo)
        #use startbacktest function so we block until finished
        train_algo_back.startbacktest(startdate=(1, 1, 2002), enddate=(1, 1, 2015))
        #train_algo_back.gui()

        #Train the model
        X_train = np.asarray(train_algo_back.features[:-1])
        y_train = np.asarray(train_algo_back.labels)

        #Write the data to file
        np.savetxt(features_name + ext, X_train, delimiter=",")
        np.savetxt(labels_name + ext, y_train, delimiter=",")
    else:
        if verbose:
            print "Loading cached data"
        X_train = np.loadtxt(features_name + ext, delimiter=",") 
        y_train = np.loadtxt(labels_name + ext, delimiter=",")

    unique, count = np.unique(y_train, return_counts=True)
    count = dict(zip(unique, count))
    if verbose:
        print "Training set stats:"
        print "====================================="
        print "UP\t" + str(count[CLASS_UP])
        print "FLAT\t" + str(count[CLASS_FLAT])
        print "DOWN\t" + str(count[CLASS_DOWN])
        print "====================================="

        print "Training classifier"

    """
    ======= CLASSIFIER TRAINING (ANALYSIS) ========
    """
    reg_val = 10000
    online = False

    if args.analysis:
        results = []
        for num_features in iterate_features:
            rkf = RepeatedKFold(n_splits = 10, n_repeats = 100)
            test_clf = None
            i = 0
            for train_idx, test_idx in rkf.split(X_train):
                if verbose:
                    i += 1
                    print "Training: " + str(i / float(rkf.get_n_splits()) * 100) + "%\t|\t(" + str(num_features) + ")"

                x_fold_train = X_train[train_idx]
                y_fold_train = y_train[train_idx]
                x_fold_train = [f[:num_features] for f in x_fold_train]

                x_fold_test = X_train[test_idx]
                y_fold_test = y_train[test_idx]
                x_fold_test = [f[:num_features] for f in x_fold_test]
                #print x_fold_test
                
                if args.clf == "all" or args.clf == "svc-rbf":
                    clf = SVC(kernel="rbf", C=reg_val)
                    clf.fit(x_fold_train, y_fold_train)
                    preds = clf.predict(x_fold_test)
                    score = accuracy_score(y_fold_test, preds)
                    precision = precision_score(y_fold_test, preds, average="macro")
                    recall = recall_score(y_fold_test, preds, average="macro")
                    # Count num predictions
                    unique, count = np.unique(preds, return_counts=True)
                    count = dict(zip(unique, count))
                    results.append( ("svc-rbf", score, precision, recall, count.get(CLASS_UP, 0), count.get(CLASS_FLAT, 0), count.get(CLASS_DOWN, 0), num_features)  )

                if args.clf == "all" or args.clf == "svc-linear":
                    clf = SVC(kernel="linear", C=reg_val)
                    clf.fit(x_fold_train, y_fold_train)
                    preds = clf.predict(x_fold_test)
                    score = accuracy_score(y_fold_test, preds)
                    precision = precision_score(y_fold_test, preds, average="macro")
                    recall = recall_score(y_fold_test, preds, average="macro")
                    # Count num predictions
                    unique, count = np.unique(preds, return_counts=True)
                    count = dict(zip(unique, count))
                    results.append( ("svc-linear", score, precision, recall, count.get(CLASS_UP, 0), count.get(CLASS_FLAT, 0), count.get(CLASS_DOWN, 0), num_features)  )

                if args.clf == "all" or args.clf == "random-forest":
                    clf = RandomForestClassifier(max_depth=9)
                    clf.fit(x_fold_train, y_fold_train)
                    preds = clf.predict(x_fold_test)
                    score = accuracy_score(y_fold_test, preds)
                    precision = precision_score(y_fold_test, preds, average="macro")
                    recall = recall_score(y_fold_test, preds, average="macro")
                    # Count num predictions
                    unique, count = np.unique(preds, return_counts=True)
                    count = dict(zip(unique, count))
                    results.append( ("random-forest", score, precision, recall, count.get(CLASS_UP, 0), count.get(CLASS_FLAT, 0), count.get(CLASS_DOWN, 0), num_features)  )

                if args.clf == "all" or args.clf == "log-regression":
                    clf = LogisticRegression(C=reg_val)
                    clf.fit(x_fold_train, y_fold_train)
                    preds = clf.predict(x_fold_test)
                    score = accuracy_score(y_fold_test, preds)
                    precision = precision_score(y_fold_test, preds, average="macro")
                    recall = recall_score(y_fold_test, preds, average="macro")
                    # Count num predictions
                    unique, count = np.unique(preds, return_counts=True)
                    count = dict(zip(unique, count))
                    results.append( ("log-regression", score, precision, recall, count.get(CLASS_UP, 0), count.get(CLASS_FLAT, 0), count.get(CLASS_DOWN, 0), num_features)  )

                if args.clf == "all" or args.clf == "perceptron":
                    online = False #I don't think this is used right in MLAlgo
                    clf = Perceptron(penalty="l2", max_iter=2000)
                    clf.fit(x_fold_train, y_fold_train)#, classes=[0,1,2])
                    preds = clf.predict(x_fold_test)
                    score = accuracy_score(y_fold_test, preds)
                    precision = precision_score(y_fold_test, preds, average="macro")
                    recall = recall_score(y_fold_test, preds, average="macro")
                    # Count num predictions
                    unique, count = np.unique(preds, return_counts=True)
                    count = dict(zip(unique, count))
                    results.append( ("perceptron", score, precision, recall, count.get(CLASS_UP, 0), count.get(CLASS_FLAT, 0), count.get(CLASS_DOWN, 0), num_features)  )

                if args.clf == "all" or args.clf == "ada-boost":
                    clf = AdaBoostClassifier()
                    clf.fit(x_fold_train, y_fold_train)
                    preds = clf.predict(x_fold_test)
                    score = accuracy_score(y_fold_test, preds)
                    precision = precision_score(y_fold_test, preds, average="macro")
                    recall = recall_score(y_fold_test, preds, average="macro")
                    # Count num predictions
                    unique, count = np.unique(preds, return_counts=True)
                    count = dict(zip(unique, count))
                    results.append( ("ada-boost", score, precision, recall, count.get(CLASS_UP, 0), count.get(CLASS_FLAT, 0), count.get(CLASS_DOWN, 0), num_features) )

        with open(args.output, "w") as f:
            csv_writer = csv.writer(f)
            csv_writer.writerow( ["classifier", "accuracy", "precision", "recall", "preds_up", "preds_flat", "preds_down", "num_features"] )
            for r in results:
                csv_writer.writerow(r)

    # Train classifier to use on out of sample test data
    if args.clf == "svc-rbf":
        clf = SVC(kernel="rbf", C=reg_val)
        clf.fit(X_train, y_train)

    if args.clf == "random-forest":
        clf = RandomForestClassifier(max_depth=9)
        clf.fit(X_train, y_train)

    if args.clf == "all" or args.clf == "log-regression":
        clf = LogisticRegression(C=reg_val)
        clf.fit(X_train, y_train)

    if args.clf == "perceptron":
        online = False #I don't think this is used right in MLAlgo
        clf = Perceptron(penalty="l2", max_iter=2000)
        clf.fit(X_train, y_train)#, classes=[0,1,2])

    if args.clf == "ada-boost":
        clf = AdaBoostClassifier()
        clf.fit(X_train, y_train)
    # END CLASSIFIER TRAINING

    if args.no_test:
        exit()

    if verbose:
        print "Running backtest"
    #Finally, run backtest using learned model
    ml_algo = MLAlgo(times=['every day'])
    ml_algo_back = AM.backtester(ml_algo)
    ml_algo_back.clf = clf
    ml_algo_back.online = online
    ml_algo_back.features_file = features_name + "-test" + ext
    ml_algo_back.labels_file = labels_name + "-test" + ext
    ml_algo_back.prices_file = labels_name + "-prices" + "-test" + ext
    if not args.regenerate:
        try:
            ml_algo_back.features_cache = np.loadtxt(ml_algo_back.features_file, delimiter=",")
        except:
            print "Could not load features cache for OOS test, will regenerate"
        try:
            ml_algo_back.prices_cache = np.loadtxt(ml_algo_back.prices_file, delimiter=",")
        except:
            print "Could not load pricing cache for OOS test, will regenerate"
    ml_algo_back.startbacktest(startdate=(2, 1, 2015), enddate=(10, 1, 2017))
    print (ml_algo_back.num_up, ml_algo_back.num_down)
    if ml_algo_back.features_cache is None:
        save(ml_algo_back, ml_algo_back.features_file, ml_algo_back.labels_file, ml_algo_back.prices_file)
    if args.confusion_matrix:
        if ml_algo_back.features_cache is None:
            ml_algo_back.features_cache = ml_algo_back.features[:-1]
        cnf_matrix = confusion_matrix(clf.predict(ml_algo_back.features_cache), ml_algo_back.labels[:-1])
        np.set_printoptions(precision=2)
        plt.figure()
        class_names = ["FLAT", "UP", "DOWN"]
        plot_confusion_matrix(cnf_matrix, classes=class_names,\
            title='Confusion matrix, without normalization (' + args.clf + ")")
        plt.figure()
        plot_confusion_matrix(cnf_matrix, classes=class_names,\
            title='Confusion matrix, with normalization (' + args.clf + ")", normalize=True)
        plt.show()
    if args.roc:
	#ROC Curve
        print "Calculating ROC curve..."
        if ml_algo_back.features_cache is None:
            ml_algo_back.features_cache = ml_algo_back.features[:-1]
	p = clf.predict_proba(ml_algo_back.features_cache)

        binary_labels = label_binarize(ml_algo_back.labels[:-1], list(xrange(0, 3)))
	roc_curve_classes = [ \
		("flat", 0), 
		("up", 1), 
		("down", 2)]

	for roc_class_name, roc_class in roc_curve_classes:
	    false_positive, true_positive, _ = \
		    roc_curve(binary_labels[:, roc_class], p[:, roc_class])
	    plt.plot(false_positive, true_positive, label=roc_class_name+ "(" + args.clf + ")")

	plt.gca().set_xlim([0, 1])
	plt.gca().set_ylim([0, 1.05])
	plt.legend(loc="lower right", prop={'size':10})
	plt.show()


    ml_algo_back.gui()

