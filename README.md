## CIS 419 - Final Project

### Installation

#### Dependencies

There are several dependencies required for this project. The first two are familiar, others are used for processing the financial data.

| Dependency | Description |
|------------|-------------|
|[scikit-learn](http://scikit-learn.org/stable/)| Machine learning library |
|[NumPy](http://www.numpy.org/) | Simplifies mathematical operations |
|[Robinhood](https://github.com/Jamonek/Robinhood)| Retrieve current equity prices/data |
|[Alpha Vantage](https://github.com/RomelTorres/alpha_vantage)| Retrieve historical equity prices |
|[empyrical](https://github.com/quantopian/empyrical) | Common financial risk and performance metrics |
|[Matplotlib](https://matplotlib.org/users/installing.html) | Provides graphs and plots |
| DesktopTrader | A trading platform currently in development, not yet released to the public. This dependency should have already been provided. If you feel you should have access and do not, contact us. |

To install the dependencies follow the commands below. This guide assumes scikit-learn and NumPy are already installed.

    # Create folder for the project
    mkdir project
    cd project
    
    # Setup virtual environment
    mkdir env
    virtualenv env/ --no-site-packages
    source env/bin/activate
    pip install --upgrade pip
    
    # Robinhood
    git clone https://github.com/Jamonek/Robinhood.git
    cd Robinhood
    pip install .
    
    # Alpha Vantage, empyrical, matplotlib, scikit-learn
    pip install alpha_vantage
    pip install empyrical
    pip install matplotlib
    pip install scikit-learn
    
#### Configuration

On first run, you will be prompted for [Robinhood](https://robinhood.com/) login credentials and an [Alpha Vantage API key](https://www.alphavantage.co/support/#api-key). After providing this information, it will be stored in a file called __creds.txt__. This is sensitive information, do not share it!

### Running the program

To run the program, simply start main. Data will be downloaded and cached after the first run. Upon invokation, the following operations are performed:

 1. Training features are downloaded or cached data is loaded
 2. Labels are generated for the training data
 3. The model is trained
 4. The model is tested on out of sample data
 5. A graph appears with the ML algorithm results compared with a benchmark
    
